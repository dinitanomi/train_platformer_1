/*
	Файл с сахаром для 17 версии

	- with
	- elif
	- to_str - для генерации строк (без рекурсии)
	- переопределение типов некоторых
	- randint/randreal
	- rad/rad2
	- max/min/clamp
	- sum/prod/normalise
	- setcol для консоли и fileExists
	- sleep без

	//*/


#ifndef ME_HPP
#define ME_HPP
#pragma once

#pragma clang diagnostic ignored "-Wold-style-cast"


#define _len(a) (sizeof(a) / sizeof(*(a)))
#define _nop asm("nop;")
#define _nop32 asm( "nop;nop;nop;nop;nop;nop;nop;nop;nop;nop;nop;nop;nop;nop;nop;nop;nop;nop;nop;nop;nop;nop;nop;nop;nop;nop;nop;nop;nop;nop;nop;nop;")

#include <x86intrin.h>
#include <stdexcept>
#include <random>



using word = short int;
using extended = long double;
using wchar = wchar_t;

using nint = __SIZE_TYPE__;    //          [8]
using uint = unsigned int;     //          [4]
using lint = long long int;    // long int [8]
constexpr auto pi = 3.1415926535897932384626433832795l;



//------------- elif + with ------------
#define elif else if

namespace {
	template <typename... _hv> struct __with_core__ {

	};


	/// Синтаксис типа with(a,b){}
	/// где a,b - структуры с методами __enter и __leave
	/// Порядок питоноподобный +a +b {...} -b -a
	template<typename... _hv>
	__with_core__(_hv...) -> __with_core__<_hv...>;

	template <typename T> struct __with_core__<T> {
		T& t;
		template <typename G>
		__with_core__(G&& s):t(s){
			t.__enter();
		}
		~__with_core__(){
			t.__leave();
		}

	};

	template<typename T>
	__with_core__(T&) -> __with_core__<T>;
	template<typename T>
	__with_core__(T&& s) -> __with_core__<T&&>;

	template <typename T,typename... _hv>
	struct __with_core__<T,_hv...> {

		__with_core__<T&> t;
		__with_core__<_hv&...> hv;
		//Запутался, тут можно постааить _hv2& - и ничего не меняется
		__with_core__(T& s, _hv... hv) :  t(s), hv(hv...) {
		}

		~__with_core__(){
		}
	};
	//И тут тоже можно всё менять
	//template<typename T,typename... hv>
	//__with_core__(T&& s,hv&...) -> __with_core__<T,hv&...>;
	template<typename T,typename... hv>
	__with_core__(T& s,hv&...) -> __with_core__<T,hv&...>;/*
	template<typename T,typename... hv>
	__with_core__(T&& s,hv&&...) -> __with_core__<T,hv...>;
	template<typename T,typename... hv>
	__with_core__(T& s,hv&&...) -> __with_core__<T,hv...>;*/
}

#define with(a...) if(__with_core__ with_instance(a);true)


namespace me {


	//------------- Немного простейшей математики ------------



	template <typename T, typename ...G>
	inline void tomax(T& a) {
	}
	///Приведение к максимуму
	template <typename T,typename B, typename ...G>
	inline void tomax(T& a,const B& b,const G&... g) {
		if (b>a) a=b;
		tomax(a,g...);
	}
	template <typename T, typename ...G>
	inline void tomin(T& a) {
	}
	///Приведение к минимуму
	template <typename T,typename B, typename ...G>
	inline void tomin(T& a,const B& b,const G&... g) {
		if (b<a) a=b;
		tomin(a,g...);
	}


	namespace implement{

		template<typename T,typename G>
		decltype(T()+G()) _max(const T& a,const G& b){
			return a>b?a:b;
		}

		template<typename T,typename G>
		decltype(T()+G()) _min(const T& a,const G& b){
			return a<b?a:b;
		}
	}

	///Максимум
	template<typename T>
	T max(const T& a){
		return a;
	}
	///Максимум
	template<typename T,typename ...G>
	decltype(T()+(G()+...)) max(const T& a,const G&... b){
		return implement::_max(a,max(b...));
	}

	///Обрезка
	template<typename T,typename F,typename G>
	void clamp(const T& a,F& x,const G& b){
		if (x<a)
			x=a;
		if (x>b)
			x=b;
	}

	///Минимум
	template<typename T>
	T min(const T& a){
		return a;
	}

	///Минимум
	template<typename T,typename ...G>
	decltype(T()+(G()+...)) min(const T& a,const G&... b){
		return implement::_min(a,min(b...));
	}

	/// Квадрат нормы
	template <typename G,typename ...T>
	inline auto rad2(const G b,const T... a){
		return b*b+((a*a)+...);
	}

	/// Модуль нормы
	template <typename G,typename ...T>
	inline auto rad(const G b,const T... a){
		return sqrt(b*b+((a*a)+...));
	}

	/// Сумма
	template <typename ...T>
	inline auto sum(const T... a){
		return (a+...);
	}

	/// Перемножение
	template <typename ...T>
	inline auto prod(const T... a){
		return (a*...);
	}

	namespace implement{
		template <typename B,typename F>
		inline void mul(B b,F& f){
			f*=b;
		}
		template <typename B,typename F,typename ...T>
		inline void mul(B b,F& f,T&... t){
			mul(b,t...);
			f*=b;
		}
	}

	///Нормализовать вектор
	template <typename ...T>
	inline void normalize(T&... a){
		auto r=sqrt(1/rad2(a...));
		static_assert (std::is_floating_point<decltype(r)>::value,"Normalize not floating point");

		implement::mul(r,a...);
	}

	///Не уверен насчёт оптимальности и не мучается ли он от создания распределения на ходу - использовать с аккуратностью
	template <typename T,typename G>
	inline decltype(T()+G()) randint(T a,G b){
		static std::default_random_engine gen(_rdtsc());
		return std::uniform_int_distribution<decltype(T()+G())>(a,b)(gen);
	}

	namespace implement{
		std::default_random_engine real_seed(_rdtsc());
	}

	///Не уверен насчёт оптимальности и не мучается ли он от создания распределения на ходу - использовать с аккуратностью - если тип не указан - то используется double
	template <typename T,typename G>
	inline
	typename std::enable_if<std::is_floating_point<decltype(T()+G())>::value,decltype(T()+G())>::type
	randreal(T a,G b){

		return std::uniform_real_distribution<decltype(T()+G())>(a,b)(implement::real_seed);
	}

	template <typename T,typename G>
	inline
	typename std::enable_if<!std::is_floating_point<decltype(T()+G())>::value,double>::type
	randreal(T a,G b){
		return std::uniform_real_distribution<double>(a,b)(implement::real_seed);
	}

	//------------- Немного работы со строками --------------
	template <typename ...a>
	inline wchar_t* to_str(const wchar_t* s,a... hv){
		static bool lock=false;
		if (lock)
			throw std::runtime_error("to_str<wchar> - recurion not allowed");
		lock=true;
		constexpr unsigned N=4096;
		static wchar_t* buf = new wchar_t[N];
		swprintf(buf,N,s,hv...);
		lock=false;
		return buf;
	}

	template <typename ...a>
	inline char* to_str(const char* s,a... hv){
		static bool lock=false;
		if (lock)
			throw std::runtime_error("to_str<char> - recurion not allowed");
		lock=true;
		constexpr unsigned N=4096;
		static char* buf = new char[N];
		snprintf(buf,N,s,hv...);
		lock=false;
		return buf;
	}

	template <typename ...a>
	inline void fill_str(wchar_t* &buf,int &n,const wchar_t* s,a... hv){
		int u=swprintf(buf,n,s,hv...);
		n-=u;
		buf+=u;
	}

	template <typename ...a>
	inline void fill_str(char* &buf,int &n,const char* s,a... hv){
		int u=snprintf(buf,n,s,hv...);
		n-=u;
		buf+=u;
	}

}



#include <chrono>
#include <thread>
#include <intrin.h>

// ------------- Немного работы со временем --------
namespace me{
	///this_thread::sleep_for - время в секундух для питоноподобности
	inline void sleep(double t){
		std::this_thread::sleep_for(std::chrono::nanoseconds(nint(t*1.0e9)));
	}

	namespace implement{
		double get_hz_cpuid(){
			int cpuInfo[4] = {0, 0, 0, 0};
			__cpuid(cpuInfo, 0);
			if (cpuInfo[0] >= 0x16) {
				__cpuid(cpuInfo, 0x16);
				return cpuInfo[0]*1e6;
			}
			return 2.4e9;
		}
		static nint start_clock = clock(), start_hz = _rdtsc();
		static nint _time[8];
	}
	///Примерная частота процессора
	///
	/// если нужно просто примерно число, можно брать raw_hz
	static double raw_hz = implement::get_hz_cpuid();

	///Примерная частота процессора
	///
	///А это с обновлением
	inline double get_hz() {
		nint _c = clock(), _h = _rdtsc();
		if (_c != implement::start_clock) {
			raw_hz = (double)(_h - implement::start_hz)*CLOCKS_PER_SEC/(_c - implement::start_clock);
		}
		return raw_hz;
	}





	///Замер времени
	///
	/// Использовать примерно как:
	///  timeit(13);
	/// ...<какие-то действия>
	/// printf("sec: %.6f\n ",timeit(13));
	inline static double timeit(unsigned char i) {
		nint t2 = _rdtsc();
		double e = (t2 - implement::_time[i]) / get_hz();
		implement::_time[i] = t2;
		return e;
	}

}




#include <windows.h>
#include <io.h>
namespace me
{
	// --------- Немного windows-кода ---------------

	inline void setcol(char a, char b) {
		static HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);

		SetConsoleTextAttribute(hConsole, (WORD)((b << 4) | (a % 16)));
	}

	inline bool fileExists(const char* fname) { return access(fname, 0) != -1; }
	inline bool fileExists(const wchar* fname) { return _waccess(fname, 0) != -1; }

}

//#ifdef me_c


//#endif

//- -- --- ---- ----- ---------------
// = == === ===== ========
//"  + ++ +++ ++++ /=/ /====/ /============/  / / // /// //// //////=
//\\\==\ |  || ||| ||||  || ||===|
//      ! !! !!! !!!!


#endif

