cmake_minimum_required(VERSION 3.5)

project(train_platformer_1)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++2a")

add_executable(${PROJECT_NAME} main.cpp game.cpp gl_utils.cpp)

target_link_libraries (${PROJECT_NAME} sfml-graphics sfml-window sfml-system sfml-network)
